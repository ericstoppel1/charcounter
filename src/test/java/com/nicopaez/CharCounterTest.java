package com.nicopaez;


import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;

import static org.hamcrest.MatcherAssert.assertThat;

public class CharCounterTest {

    @Test
    public void whenInputIsEmptyResultIsEmpty() {
        CharCounter counter = new CharCounter();
        HashMap<Character, Integer> result =  counter.countAll("");
        assert(result.isEmpty());
    }

    @Test
    public void givenStringCountIsOk(){
        char workingChar = 'o';
        String text = "some wonderful text";
        CharCounter counter = new CharCounter();
        HashMap<Character, Integer> result =  counter.countAll(text);
        Assert.assertTrue(result.containsKey(workingChar));
        Assert.assertEquals(result.get(workingChar).intValue(), 2);
        Assert.assertEquals(result.size(), 14);
    }
}
