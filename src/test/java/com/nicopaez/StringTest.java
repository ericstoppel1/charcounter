package com.nicopaez;

import org.junit.Assert;
import org.junit.Test;

public class StringTest {

    @Test
    public void getFirstCharacterOfString() {
        String phrase = "this is my phrase";
        char first = phrase.charAt(0);
        Assert.assertEquals(first, 't');
    }

    @Test
    public void lengthOfString() {
        String phrase = "this";
        Assert.assertEquals(phrase.length(), 4);
    }

    @Test
    public void containsInString(){
        String phrase = "this is my phrase";
        Assert.assertTrue(phrase.contains("my p"));
    }

    @Test
    public void LoweredCaseString(){
        String upperedPhrase = "THIS Is mY Phrase";
        String loweredPhrase = "this is my phrase";
        Assert.assertEquals(upperedPhrase.toLowerCase(), loweredPhrase);
    }

    @Test(expected = StringIndexOutOfBoundsException.class)
    public void charAtOutOfIndex(){
        String phrase = "this is my phrase";
        phrase.charAt(8888);
    }
}
