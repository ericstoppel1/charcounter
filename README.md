Base Project for trainings by @inicopaez
========================================


This is a base project for trainings. It uses the following tools:

* JUnit
* Mockito
* Cobertura

Commands:

* Compile and run tests: _mvn clean test_
* Check coverage: _mvn clean cobertura:cobertura_. The resulting report will be generated at target/site/cobertura/index.html.